#!/usr/bin/python
import sys
import os
import datetime
import mysql.connector
import requests
import functions
import random
import base64
from db_schema import strsys
from Naked.toolshed.shell import execute_js, muterun_js
import smtplib
from os.path import basename
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
#logging
#email smtp config
from_ = "zegeek94@gmail.com"
mdp = "schyzophrenie1"
subject = "Automatic report: Please don't answer - FTS Monitoring"
content = "Automatic report for configured systems - FTS Monitoring"
smtp = "smtp.gmail.com"
smtp_port = 587

#try:
#    server = smtplib.SMTP_SSL(smtp, smtp_port)
#    server.ehlo()
#    server.login(from_,mdp)
#except:
#    print("Login failed to SMTP server.")
#database access
hostname = "localhost"
username = "root"
password = ""
db = "FTS_mon_app"
max_disp = 30
FTS_mon_app = mysql.connector.connect(
	host=hostname,
	user=username,
	passwd=password,
	database=db
)
FREQ = 0
reports = functions.get_reports_by_Freq(FTS_mon_app,FREQ)
report_generator = "http://monitoring.test/report_creator.php"
#0: id
#1: name
#2: email_list
#3: freq
#4:date
#5: owner
for report in reports:
    print("Creating report %s..." % report[1])
    emails = report[2].split(",")
    entries = functions.get_report_entries(FTS_mon_app,report[0])
    id_strsys = []
    reports = []
    for entry in entries:
        id_strsys.append(entry[1])
    for id_sys in id_strsys:
        strsys = functions.get_strsys_by_id(id_sys,FTS_mon_app)
        last_pools_entries = functions.load_last_pools_entry(FTS_mon_app,id_sys)
        if strsys[1] == 1:
            last_rgs_entries = functions.load_last_rgs_entry(FTS_mon_app, id_sys)
        pools_id = functions.get_pools_by_strsys_id( id_sys,FTS_mon_app)
        if strsys[1] == 1:
            rgs_id = functions.get_rgs_by_strsys_id( id_sys,FTS_mon_app)
        dates = functions.get_dates(FTS_mon_app, strsys[1],strsys[0] )
        used = 0
        free = 0
        for pool in last_pools_entries:
            used = used + (pool[2]*100)/pool[1]
            free = free + (pool[3]*100)/pool[1]
        if strsys[1] == 1:
            for rg in last_rgs_entries:
                used = used + ((rg[1]-rg[2])*100)/rg[1]
                free = free + (rg[2]*100)/rg[1]
        fraction = len(last_pools_entries)
        if strsys[1] == 1:
            fraction = fraction  + len(last_rgs_entries)
        used = used /fraction
        free = free/fraction
        used = round(used,3)
        free = round(free,3)
        print("Generating chart..")
        script = "gen_chart.js %s %s" % (str(used),str(free))
        result = muterun_js(script)
        if result:
            name = "%s%s.png" % (str(used),str(free))
            print("chart generated under name %s" % (name))
        print("Generating graph..")
        graph_script = "const ChartjsNode = require('chartjs-node');\n"
        graph_script += "var chartNode = new ChartjsNode(700, 600);\n"
        graph_script += "var graphData1 = {\n"
        graph_script += "     labels: [\n"
        i= 0
        while len(dates) > max_disp:
            dates.pop(0)
        for date in dates:
            graph_script += "\""+str(date[0])+"\""
            if i < len(dates) - 1:
                graph_script += ", "
            i += 1
        graph_script += "], datasets : [\n"
        i = 0
        for pool in pools_id:
            color = "#"+''.join([random.choice('0123456789ABCDEF') for h in range(6)])
            graph_script += "{\n"
            graph_script += "label: \""+str(pool[1])+"\",\n"
            graph_script += "borderColor: \""+color+"\",\n"
            data = functions.load_pool_entries(FTS_mon_app,pool[0],id_sys)
            graph_script += "data : [ \n"
            m = 0
            while m < len(dates) - len(data):
                graph_script += "0, "
                m += 1
            j = 0
            while len(data) > max_disp:
                data.pop(0)
            for dt in data:
                graph_script += str(round(dt[2],3))
                if j < len(data) - 1:
                    graph_script += ", "
                j += 1
            graph_script += "], fill:false \n}"
            if i < len(pools_id) - 1 or len(rgs_id) > 0:
                graph_script += ", \n"
            i += 1
        i = 0
        if strsys[1] == 1:
            for rg in rgs_id:
                color = "#"+''.join([random.choice('0123456789ABCDEF') for h in range(6)])
                graph_script += "{"
                graph_script += "label: \"RAID GROUP "+str(rg[1])+"\",\n"
                graph_script += "borderColor: \""+color+"\",\n"
                data = functions.load_rg_entries(FTS_mon_app,rg[0], id_sys)
                graph_script += "data : [ \n"
                j = 0
                m = 0
                while m < len(dates) - len(data):
                    graph_script += "0, "
                    m += 1
                while len(data) > max_disp:
                    data.pop(0)
                for dt in data:
                    graph_script += str(round(dt[1] - dt[2],3))
                    if j < len(data) - 1:
                        graph_script += ","
                    j += 1
                graph_script += "], fill:false }"
                if i < len(rgs_id) - 1:
                    graph_script += ", \n"
                i += 1
        graph_script += "]"
        graph_script += "};\n"
        graph_script += "var graphOP = {\n"
        graph_script += "type: 'line',\n"
        graph_script += "data: graphData1,\n"
        graph_script += "options: {"
        graph_script += "    scales: {"
        graph_script += "    yAxes: [{"
        graph_script += "        ticks: {"
        graph_script += "            beginAtZero: true"
        graph_script += "        }"
        graph_script += "    }]"
        graph_script += "}"
        graph_script += "}};\n"
        graph_script += "return chartNode.drawChart(graphOP)\n"
        graph_script += ".then(() => {\n"
        graph_script += "return chartNode.getImageBuffer('image/png');\n"
        graph_script += "})\n"
        graph_script += ".then(buffer => {\n"
        graph_script += "Array.isArray(buffer);\n"
        graph_script += "return chartNode.getImageStream('image/png');\n"
        graph_script += "})\n"
        graph_script += ".then(streamResult => {\n"
        graph_script += "return chartNode.writeImageToFile('image/png', './graph.png');\n"
        graph_script += "})\n"
        graph_script += ".then(() => {\n"
        graph_script += "});\n"
        script_js = open("graph_script.js","w")
        script_js.write(graph_script)
        script_js.close()
        result = muterun_js("graph_script.js")
        if result:
            print("graph generated under name graph.png")
        with open(str(used)+str(free)+".png", "rb") as chart:
            base64chart = base64.b64encode(chart.read()).decode()
            chart.close()
        with open("graph.png","rb") as graph:
            base64graph = base64.b64encode(graph.read()).decode()
            graph.close()
        os.remove(str(used)+str(free)+".png")
        os.remove("graph.png")
        uri_chart = 'data:image/png;base64,{}'.format(base64chart)
        uri_graph = 'data:image/png;base64,{}'.format(base64graph)
        PARAMS = {'id_strsys':id_sys,'chartPie':uri_chart,'graph':uri_graph}
        response = requests.post(url = report_generator, data = PARAMS)
        if str(response.content).find("report-") != -1:
            reports.append(str(response.content))
    print("mail will be sent to:")
    print(emails)
    msg = MIMEMultipart()
    msg['From'] = from_
    msg['To'] = emails
    msg['Subject'] = subject
    body = MIMEText(content, 'plain')
    msg.attach(body)
    for f in reports:
        part = MIMEApplication('',Name=basename(f))
        part['Content-Disposition'] = 'attachment; filename="{}"'.format(basename(f))
        print(f)
        msg.attach(part)
    
if len(reports) == 0:
    print("No report to execute.")