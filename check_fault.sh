#!/bin/bash
if [ "$#" == "3" ]
then
    IP=$1
    USER=$2
    PASS=$3
    FAULTS=$(/opt/Navisphere/bin/naviseccli -User $USER -Password $PASS -Scope 0 -h $IP faults -list | wc -l)
    echo $FAULTS
fi
